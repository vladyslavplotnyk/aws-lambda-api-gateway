import json
from pydantic import BaseModel
from urllib.parse import urlparse


class GoldStandard(BaseModel):
    url: str
    description: str
    type: int


class UserSubmitted(BaseModel):
    url: str


class UploadedVideo(BaseModel):
    version: str
    gold_standard: GoldStandard
    user_submitted: UserSubmitted


class ProcessedVideo():

    def __init__(self, url, processing_time, score):
        self.url = url
        self.processing_time = processing_time
        self.score = score

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)


class S3Url(object):

    def __init__(self, url):
        self._parsed = urlparse(url, allow_fragments=False)

    @property
    def bucket(self):
        return self._parsed.netloc

    @property
    def key(self):
        if self._parsed.query:
            return self._parsed.path.lstrip('/') + '?' + self._parsed.query
        else:
            return self._parsed.path.lstrip('/')

    @property
    def url(self):
        return self._parsed.geturl()
