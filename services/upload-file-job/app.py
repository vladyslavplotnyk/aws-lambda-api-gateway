import json
import boto3
from models import UploadedVideo, ProcessedVideo, S3Url
import utils

GOLD_STANDARD_VIDEO = '/tmp/goldStandard.mp4'
USER_SUBMITTED_VIDEO = '/tmp/userSubmitted.mp4'
PROCESSED_VIDEO = '/tmp/processed.mp4'


def handler(event, context):

    try:
        # important: set batch size to 1 when creating an SQS trigger in lambda test 7
        body = json.loads(event['Records'][0]['body'])
        message = json.loads(body['Message'])

        # parse and validate incoming message
        uploaded_video = UploadedVideo(**message)

        print('Uploaded video: ', uploaded_video)

        print(utils.stringLength(uploaded_video.gold_standard.description))

        s3_client = boto3.client('s3')

        # get bucket name and key
        goldStandardUrl = S3Url(uploaded_video.gold_standard.url)
        userSubmittedUrl = S3Url(uploaded_video.user_submitted.url)

        # downlod video from the private S3 bucket
        s3_client.download_file(goldStandardUrl.bucket,
                                goldStandardUrl.key, GOLD_STANDARD_VIDEO)
        s3_client.download_file(userSubmittedUrl.bucket,
                                userSubmittedUrl.key, USER_SUBMITTED_VIDEO)

        # process videos here
        process_video(GOLD_STANDARD_VIDEO,
                      USER_SUBMITTED_VIDEO, PROCESSED_VIDEO)

        # create an SNS client
        sns_client = boto3.client('sns')

        # example of processed video message
        processed_video = ProcessedVideo(
            's3://videos-destination-bucket/sample_960x540.mp4', 30, 98)

        # publish a message to the specified SNS topic
        sns_client.publish(
            TopicArn='arn:aws:sns:us-east-2:144805114366:web-app',
            Message=processed_video.toJSON(),
        )

    except Exception as e:
        print('Error has occured: ', e)
        return {
            'statusCode': 400,
            'body': json.dumps({'error': f'invalid input: {e}'}),
        }

    return {
        'statusCode': 200
    }


def process_video(gold_standard_video, user_submitted_video, processed_video):

    print('Gold standard video: ', gold_standard_video)
    print('User submitted video: ', user_submitted_video)
    print('Processed video: ', processed_video)
