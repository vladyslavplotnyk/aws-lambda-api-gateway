import json
import os
import boto3

S3_DESTINATION_BUCKET = "videos-destination-bucket"

def lambda_handler(event, context):

    s3_source_bucket = event['Records'][0]['s3']['bucket']['name']
    s3_source_key = event['Records'][0]['s3']['object']['key']

    s3_source_basename = os.path.splitext(os.path.basename(s3_source_key))[0]
    s3_destination_filename = s3_source_basename + '.mp4'
    s3_destination_thumbnail = s3_source_basename + '.png'

    s3_client = boto3.client('s3')

    # downlod video from the private S3 bucket
    s3_client.download_file(s3_source_bucket, s3_source_key, '/tmp/' + s3_source_key)
        
    # convert video file and save to temp folder
    command='/opt/bin/ffmpeg -i /tmp/' + s3_source_key + ' -f mp4 /tmp/' + s3_destination_filename
    os.system(command)
        
    # upload video to the private S3 bucket
    s3_client.upload_file('/tmp/' + s3_destination_filename, S3_DESTINATION_BUCKET, s3_destination_filename)
    
    # generate a thumbnail and save to temp folder
    command='/opt/bin/ffmpeg -i /tmp/' + s3_source_key + ' -ss 00:00:01 -vframes 1 -filter:v scale="280:-1" /tmp/' + s3_destination_thumbnail + ' -y'
    os.system(command)
    
    # upload thumbnail to the private S3 bucket
    s3_client.upload_file('/tmp/' + s3_destination_thumbnail, S3_DESTINATION_BUCKET, s3_destination_thumbnail)

    return {
        'statusCode': 200,
        'body': json.dumps('Processing complete successfully')
    }