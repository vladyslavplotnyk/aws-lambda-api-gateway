FROM amazon/aws-lambda-nodejs:12
COPY src/app.js src/package*.json ./
RUN npm install
CMD [ "app.lambdaHandler" ]
